# README #

This is a small blog application that uses Babel and Webpack to function. You need to have NodeJS installed for this to work.

## Install the following after cloning this repo ##

* https://github.com/bradtraversy/babel_webpack_starter

Download/clone that repo and open a terminal from the directory you are working from and run ```npm install```

Now open the package.json file and add this to the scripts ```"json:server": "json-server --watch api/db.json"```

Also create a new folder called api, and within it create a file called db.json, this is basically our small API (json server) that we can work with.

In the http.js file I'm using my own HTTP controller, which has it's own repo that can be found here: https://bitbucket.org/washik/easyhttp/ if you are interested.


## Start the servers ##

Once you have webpack up and running we can open a terminal from the folder you are working in and run the command: 
```npm start```
This will run the node server, make sure to leave the terminal open.

Next open another terminal from the folder you are working in and run the command:
```npm run json:server```
Also leave this terminal open as it is running the json server.

Data will be stored in the json server and as long as both servers are running we can acces the data anytime we want even after the browser is closed, as long as the two terminals are running the servers once we try to acces the data.